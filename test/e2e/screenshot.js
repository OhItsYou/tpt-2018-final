var config = require('../../nightwatch.conf.js');

module.exports = {
    'screenshot': function(browser) {
        browser
            .url('https://www.google.ee/')
            .waitForElementPresent('body', 1000)
            .assert.title('Google')
            .assert.visible('input[type=text]')
            .setValue('input[class="gLFyf gsfi"]', ['tallinn', browser.Keys.ENTER])
            .pause(3000)
            .assert.containsText('body', 'tallinn')
            .saveScreenshot(config.imgpath(browser) + 'firstresult.png')


            .click('h3[class="LC20lb"]')
            .pause(1000)
            .saveScreenshot(config.imgpath(browser) + 'secondresult.png')
            .end();
    }
};
